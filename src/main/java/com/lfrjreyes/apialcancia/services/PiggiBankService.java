package com.lfrjreyes.apialcancia.services;

import com.lfrjreyes.apialcancia.domain.dto.CoinDto;
import java.io.IOException;

public interface PiggiBankService {
  boolean addCoin(CoinDto coinDto) throws Exception;
  Integer countCoin();
  Integer countCountByDenomination(CoinDto coinDto) throws IOException;
  Integer sumAllCoins();
  Integer sumCoinByDenomination(CoinDto coinDto) throws IOException;
  void cleanPiggyBank();
}
