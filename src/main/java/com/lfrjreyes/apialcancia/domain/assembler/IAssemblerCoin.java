package com.lfrjreyes.apialcancia.domain.assembler;


import com.lfrjreyes.apialcancia.domain.dto.CoinDto;
import com.lfrjreyes.apialcancia.domain.model.Coin;
import org.mapstruct.Mapper;

@Mapper
public interface IAssemblerCoin {

  CoinDto toDto(Coin coin);
  Coin toEntity(CoinDto coinDto);


}
