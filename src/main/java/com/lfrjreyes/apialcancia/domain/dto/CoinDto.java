package com.lfrjreyes.apialcancia.domain.dto;


import com.lfrjreyes.apialcancia.domain.enums.coinType.CoinValorConverter;
import javax.persistence.Convert;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.FieldDefaults;

@Getter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@FieldDefaults(level = AccessLevel.PRIVATE)
public class CoinDto {

  @Convert(converter = CoinValorConverter.class)
  private Integer valor;

}
