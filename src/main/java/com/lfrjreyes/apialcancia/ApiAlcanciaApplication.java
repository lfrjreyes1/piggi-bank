package com.lfrjreyes.apialcancia;

import com.lfrjreyes.apialcancia.config.SwaggerConfig;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;

@SpringBootApplication
@Import(SwaggerConfig.class)
public class ApiAlcanciaApplication {

	public static void main(String[] args) {
		SpringApplication.run(ApiAlcanciaApplication.class, args);
	}

}
