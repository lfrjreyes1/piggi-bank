PIGGY BANK
Piggy Bank es un proyecto del funcionamiento de una alcancia
para una prueba tecnica para una empresa X.

Comenzando 🚀
repositorio GIT
git clone https://lfrjreyes1@bitbucket.org/lfrjreyes1/piggi-bank.git

Pre-requisitos 📋
Java 8
JDK
Maven

Despliegue 📦
se debe descargar el repositorio y luego subir
el servicio con el ide de su preferencia.
La aplicacion corre sobre el puerto 8090

puede probar que la aplicacion esta arriba entrando a la documentacion
en el siguiente link
http://localhost:8090/swagger-ui.html


Construido con 🛠️

Spring Boot - El framework web usado
Maven - Manejador de dependencias

Autor ✒️

Luis Fernando Reyes Jaraba
